// ignore: unused_import
import 'package:flutter/material.dart';
import 'package:my_first_project/screens/exterior_features_screen.dart';
import 'package:my_first_project/theme/custom_theme.dart';

class InteriorFeatures extends StatelessWidget {
  const InteriorFeatures({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
        return MaterialApp(
      title: 'My First Project',
      debugShowCheckedModeBanner: false,
      theme: CustomTheme.lightTheme,
      home: Scaffold(
          appBar: AppBar(
          title: const Text('Interior Features'),
          leading: GestureDetector(
            child: const Icon( Icons.arrow_back_ios, color: Colors.yellow,),
          onTap: () {
            Navigator.pop(context);
          } ,
          ),  
        ),
        body: const Center(
          child: Text('Interior features will appear on this page'),
        ),
        floatingActionButton: FloatingActionButton(
          tooltip: 'Go to Next Page',
          child: const Icon(Icons.navigate_next),
        onPressed: (){
          Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ExteriorFeatures()));
         
        }),
        
      ),
      );
    }
}