import 'package:flutter/material.dart';
import 'package:my_first_project/theme/custom_theme.dart';

class ProfileEditScreen extends StatelessWidget {
  const ProfileEditScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'My First Project',
      theme: CustomTheme.lightTheme,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Profile Edit Page'),
          leading: GestureDetector(
          child: const Icon( Icons.arrow_back_ios, color: Colors.yellow,),
          onTap: () {
            Navigator.pop(context);
          } ,
        ),
        ),
        body: const Center(
          child: Text('Edit your profile on this page'),
        ),
      ),
    );
  }
}   