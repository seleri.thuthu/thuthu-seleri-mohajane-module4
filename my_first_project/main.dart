import 'package:flutter/material.dart';
import 'package:my_first_project/screens/log_in_screen.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:my_first_project/theme/custom_theme.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: CustomTheme. lightTheme,
      debugShowCheckedModeBanner: false,
        title: 'My First Project',
        home: AnimatedSplashScreen(
        duration: 3000,
        splash: Icons.home,
        nextScreen: const LogInScreen(),
        splashTransition: SplashTransition.fadeTransition,
        backgroundColor: Colors.blueGrey
        ),
       );
      
  }
}
