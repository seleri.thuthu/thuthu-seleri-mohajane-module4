import 'package:flutter/material.dart';

class CustomTheme {
  static ThemeData get lightTheme { //1
    return ThemeData( //2
      primarySwatch: Colors.lightGreen,
      primaryColor: Colors.purple,
      scaffoldBackgroundColor: Colors.lightBlue[50],
      // ignore: deprecated_member_use
      accentColor: Colors.lime,
      fontFamily: 'Montserrat', //3
      buttonTheme: ButtonThemeData( // 4
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0)),
        buttonColor: Colors.black,
      ), 
      
    );
  }
}